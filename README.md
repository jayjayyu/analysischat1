				

Brief And Rushed Through Secure Programming Hackchat Vulnerability Report

By: Yu - (Vunet ID: jyu290)

					

Vulnerability 1A  (program 4)
---------------------------

Type:  Authentication error

Impact: 

1. Confidentiality: Mallory can do a Man In The Middle (MITM)  attack and have a fake chat server to see the contents of all the private messages that were not end-to-end encrypted and Mallory can intercept all the passwords to login on the real server as well. 

2. Integrity: Mallory can intercept and modify and/or delete all the private messages that were not encrypted and were sent through the fake chat server as well, before forwarding them to the client and/or the real chat server.

Points estimation: 2 (first vulnerability of type) * 2.5 ( both confidentiality and integrity ) * 2 (steps to reproduce with full impact) = 10

Cause: The client does not verify the server certificate is signed by the Trusted Third Party (root certificate authority).
As between “SSL *ssl = SSL_new(ctx)” (line 165 of src/client/client.c) 

and “ssl_block_connect(ssl, fd);” (line 170 of src/client/client.c),

they don’t load in the Trusted Third Party certificate using SSL_CTX_load_verify_locations 

and they also don’t verify the Trusted Third Party certificate by using the SSL_set_verify function 

with the SSL_VERIFY_PEER option.

This allows any certificate to be used at the server and the client won’t verify that the certificate is authentic,

this allows a Man-in-the-Middle to present a fake SSL certificate for the chatserver and the client won’t notice that the certificate is not signed by the TTP.


Steps:

1. Remember that the attacker called Mallory can “Read, modify, inject, and/or block data sent over any network connection between clients and the server." (see threadmodel in section 6.1 of the secchat document).
2. Let Mallory set up a fake chat server with a server certificate (e.g. “mallory.evil.com”) that’s not signed by the trusted third party that allows Mallory to intercept and modify all the messages that are sent between the client and the server over an encrypted TLS connection as a so-called “active attacker”.
3. Let all the chatclients connect to Mallory’s fake chatserver over TLS, as the client don’t verify that the certificate is signed by the Trusted Third Party, this MITM attack connection will be established as usual.
4. All the chatclients will then connect fine to Mallory’s fake chatserver over TLS and now think it’s the real chatserver. 
5. Let all the clients send passwords and private messages to Mallory’s fake chatserver.
6. Let Mallory intercept all the passwords and private messages from the client to break confidentiality and let Mallory replace all the private message content by modified private message content to break integrity as well.

Found through: Doing a “grep -nr 'SSL_VERIFY_PEER' .” on the main directory of the source codes, and checking which programs don’t do this verifications.


Vulnerability 2 (program 5)
---------------------------

Type: SQL injection

Impact: Confidentiality and Integrity break through SQL injection.

Points estimation: 2 (first vulnerability of type) * 2.5 (integrity and confidentiality) * 2 (steps to reproduce with full impact) = 10

Cause: The server tries to escape the SQL queries with the function <code>*get_escaped_string(char *input_string)</code> (in line 166 in src/common/string.h)

But it replaces the apostrophe by another apostrophe that is not escaped at all in line 172-173 of src/common/string.h:


        case '\'': {
                new_string[size] = '\'';


This would lead to errors that are not handled correctly when inputting the ‘ character does not lead to correct syntax for SQL injections. Such as “SQL Insert Message error: syntax error”.

And makes sent message not stored correctly in the database without handling it correctly. 
Not escaping input correctly could lead to SQL injection attacks as explained further below.

Steps: Register 3 users. One user called Alice (“/register Alice Alicespassword”) and one called Bob (“/register Bob Bobspassword”) and one called Eve (“/register Eve Evespassword”).

And then login as Eve (“/login Eve Evespassword”) .

And when you try to send messages containing the ‘ character that do not lead to completely correct SQL queries. This would results into incorrectly handled error messages on the server (see debug messages on the console on the server).

But it can also lead to SQL injections, for example when **Eve** enters the message:

<code>I am Bob from the year 1984', 'Bob', 0, '1984-12-01 15:19:07');--</code>

The server would recognise this as a valid message from Bob (instead of Eve) with a timestamp from 1984: 

```
1984-12-01 15:19:07 Bob: I am Bob from the year 1984
```

The SQL injection can break private message confidentiality too by forwarding the private messages to everyone in the chat, 
by simply toggling the  `is_private` field 

in the database table `Messages` from true (1) to false (0). 

So if Alice log-ins and  sends Bob a private message: 

<code><strong>@Bob Hey Bob, this is a message from Alice that Eve should never be able to see.</strong></code>


```
2019-12-14 15:52:05 Alice: @Bob Hey Bob, this is a message from Alice that Eve should never be able to see.
```


And then Eve logs in and does the SQL injection in the chat as follows:


<code>I will turn all your private messages into public messages.', 'Eve', 0, '2000-01-01 15:19:07'); UPDATE Messages SET is_private = 0;--</code>

Then Eve gets the following response back from the server, which contains the contents of Alice’s private message:

```
2019-12-15 15:52:05 Alice: Hey Bob, this is a message from Alice that Eve should never be able to see.
2000-01-01 15:19:07 Eve: I will turn all your private messages into public messages.
```

The SQL injection can break integrity of the messages too when Eve types: 

<code>I corrupt message integrity.', 'Eve', 0, '1984-01-01 15:19:07');UPDATE Messages SET message = '[MESSAGE_CORRUPTED_BY_EVE]';--</code>


Now the integrity of the messages in the database are broken, when you logout and login again, the messages get retrieved from the database, and every message should say  "**[MESSAGE_CORRUPTED_BY_EVE]**".


Found through: Manual code inspection

Vulnerability 3A (program 5)
---------------------------


Type:  Cryptography missing where required for private messages.

Impact: confidentiality and integrity
Points estimation: 2 (first vulnerability of type) * 2 (both confidentiality and integrity) * 2 (steps to reproduce with full impact) = 10

Cause: No end-to-end encryption is implemented for private messages. No message signatures are created and verified by the clients for the private messages.

And remember that Mallory is allowed to "Implement a malicious server and get clients to connect to it instead of the intended server"

So Mallory can set-up a malicious server to see all the private messages that clients have sent, as they are not encrypted, and therefore break confidentiality. And Mallory can also modify the sender and the content of all the private messages, and therefore breaking integrity of the private messages.

Steps: Let Mallory "Implement a malicious server and get clients to connect to it instead of the intended server".

Let clients login on this malicious server and send some private messages for other offline clients.

Read the plaintext messages that are sent to the malicious server directly or open the database table containing the private 
messages on the malicious server to read the content of the private messages (as they are not encrypted by the client) and 
break confidentiality of the private messages.

Then modify these private messages to break integrity as well. Modify the private message by deleting some messages, changing words in some other words. inserting
some fake private messages, etcetera.

When the offline client who are the recipient of these fake private message login, they will think that these fake private messages are the real private messages sent by the real users.

Found through: Doing greps on OpenSSL keywords required for end-to-end encryption.

Vulnerability 4 (program 2)
---------------------------
Type:  Authorization error

Impact: mostly confidentiality, partially integrity when taking into account corrupting sent and received messages for the webclient.

Points estimation: 2 (first vulnerability of type) * 2 (mostly confidentiality) * 2 (steps to reproduce with full impact) = 8

Cause:  When a client manually activates the private test webserver on [https://localhost:8080](https://localhost:8080) it authorizes everyone who connects to this private webserver to see all the messages that has been sent to the client (including private messages) without requiring additional password/authentication-code to see the messages. 

Steps: Run a private webserver through the client with command “./client -w”, login and receive some messages from others. Then connect to the private webserver: [https://localhost:8080](https://localhost:8080) 
And see all the sent and received messages.

Found through: Manual code inspection.

Vulnerability 5 (program 6)
---------------------------

Type: Error handling incorrect or missing


Impact: Integrity, because the message table can be dropped, and therefore corrupt messages. And potentially confidentiality, because the recipients of the message and their certificate can be changed in the database.
Points estimation: 2 (first vulnerability of type error handling after the SQL injection) * 2 (integrity) * 2 (steps to reproduce with full impact for breaking integrity) = 8
Cause: The server is using mprint to concatenate SQL query strings instead of using prepared statements.

Steps: Login on the server with a client an do an SQL injection ont he server such as dropping tables such as the ones for the public messages:


<code>mytext', 'mytime'); DROP TABLE PUBLICMESSAGE; DROP TABLE PRIVATEMESSAGE; DROP TABLE USER;--</code>

When you logout and login again with the client on the server, and then try to send more messages, the server will show this message in the console log:


```
"database error: no such table: PUBLICMESSAGE" 
```

indicating that the SQL injection to break integrity was successful. And the server is not able to handle this error correctly without a restart of the server, and is no longer able to send and receive messages from and to clients until the server restarts. 


Found through: Manual code inspection.

Vulnerability 6 (program 4)
---------------------------

Type: NULL pointer dereference

Impact: Availability of the server.

Points estimation: 2 (first vulnerability of type) * 1.5 (availability) * 2 (steps to reproduce with full impact) = 6


Cause: The input parser is not handling the commands correctly. The server crashes when a clients send a message with only 4 whitespaces “    “, as whitespaces are handled as delimiters and it doesn’t handle empty arguments correctly. 

As shown in controller.c, it takes whitespace as delimiter, then it assigns the NULL to the firstWord (line 451 in src/server/controller.c):

<code>
char delim[] = " ";
char *firstWord = strtok(inbuf, delim);
</code>
Then it tries to do a strncmp comparison on the NULL:

<code>else if (strncmp(firstWord, "/register", 10) == 0)</code>

Found through: Injecting commands into the client application and manual code inspection.


Vulnerability 7 (program 4)
---------------------------

Type: command/argument injection (Command parsing bug)
(Second Type: Authorization error. )

Impact: confidentiality 

Points estimation: when it’s considered an input parsing error: 2 (first vulnerability of the command parsing type) * 2 (confidentiality) * 2 (steps to reproduce with full impact) = 8. Or 8/2=4 points when it’s considered to be a second authorization error.

Cause: strncmp(firstWord, **"@"**, 2) in line 471 of src/server/controller.c expects a comparison to 2 characters, while “@” is only 1 character, so never returns 0. So the code would no longer pass the go to else if (strncmp(firstWord, "@", 2) == 0) case and go the else case and forward all the private messages to everyone in the chat as a public message, which would break the confidentiality of the private messages: 


```
    else if(strncmp(firstWord, "@" 2) == 0)
    {
        send_private_message(connection, server, copiedMessage);
    }
    else
    {
        send_public_message(connection, server, copiedMessage, (*server)->db);
    }
```


Steps: Login with 3 users Alice, Bob and Eve.

Make alice send a private message to bob such as “@Bob Our shared secret password is `ki9Lo0pQ!@m.`” Now Eve will receive the private message with the secret password `ki9Lo0pQ!@m.` as public message well. Which breaks the confidentiality of the private messages.

Found through: Manual code inspection


Vulnerability 8 (program 5)
---------------------------
Type: 1. Integer overflow
(Second type: Error checking)

Impact: None (maybe availability if you expected the server to be either at a given port number or not available at all)

Points estimation: 2 (first vulnerability of type) * 1 (none) * 2 (steps to reproduce with full impact) = 4.

Cause: Instead of giving an error when a port outside the acceptable port range on the server program, the port number is 
converted to an integer and then cast to an unsigned short, making it range wrap-around:


```
port = (unsigned short)atoi(argv[1]);
```



So incorrect error handling by overflowing the port number on the server instead of alerting the user of entering incorrect input, with no direct security impact.


Steps: Start server with `./server 65536`


Which is 1 more than the maximum port number of 65535.

Open a different window and check the ports on the server listens using the command:


```
sudo netstat -lntup | grep server
```
The server might now be bound to a random port number and not be available at an expected port number.
Found through: Manual code inspection

Vulnerability 9 (program 5)  [a.k.a. Vulnerability 1B]
---------------------------


Type: Authentication error
Impact: 


1. Confidentiality: Mallory can do a Man In The Middle (MITM)  attack and have a fake chat server to see the contents of all the private messages that were not end-to-end encrypted and Mallory can intercept all the passwords to login on the real server as well. 


2. Integrity: Mallory can intercept and modify and/or delete all the private messages that were not encrypted and were sent through the fake chat server as well, before forwarding them to the client and/or the real chat server.


Points estimation: 1 (second vulnerability of type) * 2.5 ( both confidentiality and integrity ) * 2 (steps to reproduce with full impact) = 5


    					


Cause: The client does not verify the server certificate is signed by the Trusted Third Party (root certificate authority).


As in the function setup_ssl_client (line 26 of src/common/ssl.c) between “SSL *ssl = SSL_new(ctx)”  (line 27 of src/common/ssl.c) 

and “ssl_block_connect(ssl, fd);” (line 40 of src/common/ssl.c),

they don’t load in the Trusted Third Party certificate using SSL_CTX_load_verify_locations 
and they also don’t verify the Trusted Third Party certificate by using the SSL_set_verify function 
 with the SSL_VERIFY_PEER option.


This allows any certificate to be used at the server and the client won’t verify that the certificate is authentic, this allows a Man-in-the-Middle to present a fake SSL certificate for the chatserver and the client won’t notice that the certificate is not signed by the TTP.

Steps:

1. Remember that the attacker called Mallory can “Read, modify, inject, and/or block data sent over any network connection between clients and the server." (see threadmodel in section 6.1 of the secchat document).
2. Let Mallory set up a fake chat server with a server certificate (e.g. “mallory.evil.com”) that’s not signed by the trusted third party that allows Mallory to intercept and modify all the messages that are sent between the client and the server over an encrypted TLS connection as a so-called “active attacker”.
3. Let all the chatclients connect to Mallory’s fake chatserver over TLS, as the client don’t verify that the certificate is signed by the Trusted Third Party, this MITM attack connection will be established as usual.
4. All the chatclients will then connect fine to Mallory’s fake chatserver over TLS and now think it’s the real chatserver. 
5. Let all the clients send passwords and private messages to Mallory’s fake chatserver.
6. Let Mallory intercept all the passwords and private messages from the client to break confidentiality and let Mallory replace all the private message content by modified private message content to break integrity as well.

Found through: Doing a “grep -nr 'SSL_VERIFY_PEER' .” on the main directory of the source codes, and checking which programs don’t do this verifications.


					

Vulnerability 10  (program 4) [a.k.a. Vulnerability 3B]
---------------------------

Type:  Cryptography missing where required for private messages.

Impact: confidentiality and integrity
Points estimation: 1 (second vulnerability of type) * 2 (both confidentiality and integrity) * 2 (steps to reproduce with full impact) = 5

Cause: No end-to-end encryption is implemented for private messages. No message signatures are created and verified by the clients for the private messages.
And remember that Mallory is allowed to "Implement a malicious server and get clients to connect to it instead of the intended server"

So Mallory can set-up a malicious server to see all the private messages that clients have sent, as they are not encrypted, and therefore break confidentiality. And Mallory can also modify the sender and the content of all the private messages, and therefore breaking integrity of the private messages.

Steps: Let Mallory "implement a malicious server and get clients to connect to it instead of the intended server".
Let clients login on this malicious server and send some private messages for other offline clients.
Read the plaintext messages that are sent to the malicious server directly or open the database table containing the private messages on the malicious server to read the content of the private messages (as they are not encrypted by the client) and break confidentiality of the private messages.

Then modify these private messages to break integrity as well. 
Modify the private message by deleting some messages, changing words in some other words. Inserting some fake private messages, etcetera.

When the offline client who are the recipient of these fake private message login, they will think that these fake private messages are the real private messages sent by the real users.

Found through: Doing greps on OpenSSL keywords required for end-to-end encryption.

Vulnerability 11 (program 6) 
---------------------------

Type: memory leak

Impact: potential availability

Point estimation: 2 (first vulnerability of type) * 1.5 (availability) * 1 (valid explanation but no steps at all to reproduce) = 3.

Cause: In server.c of program 6 some database functions allocate memory the authors didn’t notice had to be freed.
For example, they do a “sql = sqlite3_mprintf”, which does a malloc, which is then never freed using a sqlite3_free(sql), which causes memory leaks for each sql query, as explained in the documentation:

"The sqlite3_mprintf() and sqlite3_vmprintf() routines write their results into memory obtained from sqlite3_malloc64(). The strings returned by these two routines should be released by sqlite3_free()." - [https://www.sqlite.org/draft/c3ref/mprintf.html](https://www.sqlite.org/draft/c3ref/mprintf.html)

Steps: Run the server with valgrind enabled to check for memory leaks. Then login and logout as many times as needed with the client to cause memory leaks that affect the availability of the server.

Found through: Valgrind


![ValgrindMemoryLeakScreenshot](ValgrindMemoryLeakScreenshot.png "ValgrindMemoryLeakScreenshot")


Vulnerability 12 (program 5)
---------------------------


Type: buffer overflow/overread

Impact: potential code execution when having an exploit for it (potential data corruption otherwise, and availability when program is run with -fsanitize=address)

Point estimation: 2 (first vulnerability of type) * 2.5 (potential impact is code execution) * 1 (valid explanation but absolutely no steps at all to reproduce) = 5.

Cause: In src/common/string.c at line 170 inside the function get_escape_string, it expects the input_string to be a null terminated string, but if there’s no null at the end, it could overrun it:

```
for (int i = 0; input_string[i] != 0; i++) 
```



When an important memory pointer or a return address is known or leaks and can be overwritten, it could in some cases lead to code execution.


Steps: Compile with the server application with the -fsanitize=address option. Connect the client to the server, give large size inputs, which the server calls get_escape_string on the input, it could lead to heap buffer overflow/overrun as reported by AddressSanitizer:


![AsanOverflowScreenshot](AsanOverflowScreenshot.png "AsanOverflowScreenshot")

No further explanations is given here due to time limitations.
Found through: Address Sanitizer


